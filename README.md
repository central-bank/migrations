## Flow (feature branch)

git checkout -b v1

[add migrations]

## Test (still in branch)

docker ps --all

docker stop <id>

docker inspect -f '{{ json .Mounts }}' <id>

docker rm <id>

docker volume rm <id>

docker build -t maniaque/cbmigrations:test . 

docker-compose up --force-recreate

## Merge

git add .

git commit -m '...'

git push origin v1

Merge out there somewhere :)

## Retag

git checkout master

git pull origin master

docker build -t maniaque/cbmigrations:latest -t maniaque/cbmigrations:v1 .

## Push

docker push maniaque/cbmigrations

## Debug

docker run -it --entrypoint /bin/bash maniaque/cbmigrations:test (or :latest)