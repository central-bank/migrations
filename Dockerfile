FROM flyway/flyway:latest

ADD ./migrations /flyway/sql

ENTRYPOINT sleep ${FLYWAY_SLEEP:-1} && /flyway/flyway migrate
